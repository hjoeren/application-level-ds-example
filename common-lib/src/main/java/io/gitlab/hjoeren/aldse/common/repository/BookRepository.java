package io.gitlab.hjoeren.aldse.common.repository;

import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import io.gitlab.hjoeren.aldse.common.domain.Book;

@RequestScoped
@Transactional
public class BookRepository {

  @PersistenceContext
  EntityManager entityManager;
  
  public List<Book> findAll() {
    CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
    CriteriaQuery<Book> criteriaQuery = criteriaBuilder.createQuery(Book.class);
    Root<Book> from = criteriaQuery.from(Book.class);
    criteriaQuery.select(from);
    return entityManager.createQuery(criteriaQuery).getResultList();
  }
  
  public Book save(Book book) {
    if (book.getId() == null) {
      entityManager.persist(book);
    } else {
      book = entityManager.merge(book);
    }
    return book;
  }
  
}
