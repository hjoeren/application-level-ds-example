package io.gitlab.hjoeren.aldse.api.book;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import io.gitlab.hjoeren.aldse.common.domain.Book;
import io.gitlab.hjoeren.aldse.common.repository.BookRepository;

@Path("books")
public class BooksResource {

  @Inject
  BookRepository bookRepository;

  @Context
  UriInfo uriInfo;

  @GET
  public List<Book> getAll() {
    return bookRepository.findAll();
  }

  @POST
  public Response create(@FormParam("title") String title, @FormParam("author") String author) {
    Book book = new Book(title, author);
    book = bookRepository.save(book);
    return Response.status(Response.Status.OK).entity(book.getId()).build();
  }

}
